const path = require('path')

module.exports = {
  entry: './src/rivraddon.ts',
  mode: 'development',
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  },
  devServer: {
    contentBase: path.join( __dirname, './src' ),
    compress: true,
    port: 8080
  },
  output: {
    filename: 'rivraddon.js',
    path: path.resolve( __dirname, './src' )
  }
}