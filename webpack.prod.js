const path = require('path')
const CopyPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
  entry: './src/rivraddon.ts',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  },
  output: {
    filename: 'rivraddon.js',
    path: path.resolve( __dirname, './build' )
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin([{from: './src/index.html'}])
  ]
}