const express = require('express')
const app = express()
const path = require('path')

app.get( '/*.js', (req, res) => {
  res.sendFile( path.resolve( __dirname, '../build' + req.originalUrl) )
} )

app.get( '/', (req, res) => {
  res.sendFile( path.resolve( __dirname, '../build/index.html') )
} )

app.listen( 8080, () => {
  console.log( 'App listening on localhost:8080' )
} )