# Simplaex Coding Challenge

## Getting started
Clone the project
```bash
git clone https://wagnermeyer@bitbucket.org/wagnermeyer/simplaex-coding-challenge.git
```

Install the dependencies

```bash
# npm way
npm install

# yarn way
yarn
```

### Developing
To start developing the application, you can run the commands below. This will start a webpack-dev-server, accessible on `localhost:8080`

```bash
# npm way
npm start

# yarn way
yarn start
```

### Building
To build the final bundle, run the commands below:
```bash
# npm way
npm run build

# yarn way
yarn run build
```

### Serving
If you want to check the build, you can run the commands below and it will run an express app to serve the application. Also accessible on `localhost:8080`
```bash
# npm way
npm run serve

# yarn way
yarn run serve
```

### Testing
```bash
# npm way
npm test

# yarn way
yarn test
```

Checking the coverage:
```bash
# npm way
npm test -- --coverage

# yarn way
yarn test --coverage
```

## Technical information
As the instructions document suggests to ignore comments like “DO NOT EDIT BELOW THIS LINE”, I've changed only one place. Here's the code BEFORE my changes:

```diff
  var pbjs = pbjs || {};
      pbjs.que = pbjs.que || [];
      pbjs.que.push(function() {
          pbjs.addAdUnits(adUnits);
          pbjs.requestBids({
              bidsBackHandler: initAdserver,
              timeout: PREBID_TIMEOUT
          });
      });
```

And here's the code AFTER my changes:
```diff
  var pbjs = pbjs || {};
      pbjs.que = pbjs.que || [];
      pbjs.que.push(function() {
          pbjs.addAdUnits(adUnits);
          pbjs.requestBids({
              bidsBackHandler: initAdserver,
              timeout: PREBID_TIMEOUT
          });
+         pbjs.enableAnalytics({
+             provider: 'rivr',
+             options: {
+                 clientID: 'testChallengeClientId',
+                 authToken: 'testChallengeauthToken',
+                 bannersIds: [],
+                 siteCategories: []
+             }
+         });
      });
```