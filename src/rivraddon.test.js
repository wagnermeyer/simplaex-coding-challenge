import rivraddon from './rivraddon'

describe( 'rivraddon', () => {
  it( 'should set `rivraddon` as global if window is defined', () => {
    expect( rivraddon ).toBeDefined()
  } )
} )