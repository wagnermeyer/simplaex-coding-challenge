import enableAnalytics from './analytics/enableAnalytics/enableAnalytics'
import trackPbjsEvent from './analytics/trackPbjsEvents/trackPbjsEvent'

const rivraddon = {
  analytics: {
    enableAnalytics,
    trackPbjsEvent
  }
}

declare global {
  interface Window {
    rivraddon: object
    pbjs: object
  }
}

if ( window ) {
  window.rivraddon = rivraddon
}

export default rivraddon