import { post } from './fetch'

describe('post()', () => {

  beforeEach(() => {
    fetch.resetMocks()
  })

  it( 'should POST to "https://tracker.simplaex-code-challenge.com"', async () => {
    fetch.mockResponseOnce( JSON.stringify( { } ) )
    await post( { mock: 'mock' } )
    expect( fetch.mock.calls[0][0] ).toEqual( 'https://tracker.simplaex-code-challenge.com' )
    expect( fetch.mock.calls[0][1].method ).toEqual( 'POST' )
  })

  it( 'should resolve if success', async () => {
    fetch.mockResponseOnce( JSON.stringify( { successMock: 'ok' } ) )
    const response = await post( { mock: 'mock' } )
    expect( response ).toEqual( { successMock: 'ok' } )
  } )

  it( 'should reject if error', async () => {
    fetch.mockRejectOnce( JSON.stringify( { errorMock: 'error' } ) )
    try { await post( { mock: 'mock' } )
    } catch ( error ) {
      expect( error ).toEqual( JSON.stringify({ errorMock: 'error' }) )
    }
  } )

})