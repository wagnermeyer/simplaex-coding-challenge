const apiURL:string = 'https://tracker.simplaex-code-challenge.com'

/**
 * @function post
 * @description Send a payload to the api
 * @param payload The payload that will be sent
 * @returns {Promise}
 */
const post = ( payload: object ) =>
  new Promise( ( resolve, reject ) => {
    // Using native fetch to prevent using an entire library
    fetch( apiURL, {
      method: 'POST',
      body: JSON.stringify( payload )
    } )
      .then( response => {
        response.json()
          .then( data => resolve( data ) )
      } )
      .catch( error => {
        return reject( error ) } )
  } )

export { post }