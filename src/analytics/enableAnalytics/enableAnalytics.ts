/**
 * @function enableAnalytics
 * @description Logs a message on console
 * @returns {undefined}
 */
const enableAnalytics = ():void => {
  console.log('SIMPLAEX CODE CHALLENGE LOG rivraddon analytics.enableAnalytics');
}

export default enableAnalytics