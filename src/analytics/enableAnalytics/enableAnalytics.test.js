import enableAnalytics from './enableAnalytics'

global.console = {
  log: jest.fn()
}

describe('enableAnalytics()', () => {
  it( 'should show "SIMPLAEX CODE CHALLENGE LOG rivraddon analytics.enableAnalytics" message on console', () => {
    enableAnalytics()
    expect( global.console.log ).toHaveBeenCalledWith('SIMPLAEX CODE CHALLENGE LOG rivraddon analytics.enableAnalytics')
  } )
})