import { post } from '../../utils/fetch'

/**
 * @function trackPbjsEvent
 * @description Send the prebid event to the Simplaex tracker
 * @params {object} eventObj - The prebid event object
 * @params {string} eventObj.eventType - The prebid event type
 * @returns {undefined}
 */
const trackPbjsEvent = ( eventObj: { eventType: string } ): void => {

  try {

    // Checking if eventObj.eventType is set
    if ( !eventObj ) throw Error( 'eventObj is not defined' )
    if ( !eventObj.eventType ) throw Error ( 'eventType inside eventObj object is not defined' )

    // We are good to go
    // Setting the body
    const { eventType } = eventObj
    const body = { eventType }

    // Posting the body
    post( body )

  } catch ( error ) {
    // Showing error
    console.error( error )
  }

}

export default trackPbjsEvent