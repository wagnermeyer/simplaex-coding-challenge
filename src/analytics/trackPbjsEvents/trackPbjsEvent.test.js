import trackPbjsEvent from './trackPbjsEvent'
import * as myFetch from '../../utils/fetch'

describe( 'trackPbjsEvent()', () => {

  it( 'should log an error if eventObj is not defined', () => {
    console.error = jest.fn()
    trackPbjsEvent()
    expect( console.error ).toBeCalledWith( Error('eventObj is not defined') )
  } )

  it( 'should log an error if eventObj.eventType is not defined', () => {
    console.error = jest.fn()
    trackPbjsEvent( {} )
    expect( console.error ).toBeCalledWith( Error('eventType inside eventObj object is not defined') )
  } )

  it( 'should call post() function', () => {
    myFetch.post = jest.fn()
    trackPbjsEvent( { eventType: 'mock' } )
    expect( myFetch.post ).toHaveBeenCalledWith( { eventType: "mock" } )
  } )

} )